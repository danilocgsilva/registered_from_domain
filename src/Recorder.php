<?php

namespace danilocgsilva;

class Recorder
{
    /**
     * Records user to database
     *
     * @param int $user_id
     * @return void
     */
    public function record($user_id)
    {
        $domain = $this->_returnDomain();
        \update_user_meta($user_id, 'registering_domain', $domain);
    }

    /**
     * Returns the domain in the desired format
     *
     * @return string
     */
    private function _returnDomain()
    {
        $siteUrl = get_site_url();
        return preg_replace('/^(http|https):\/\//', '', $siteUrl);
    }
}