<?php

namespace danilocgsilva;

class TableModifier
{
    const NEW_COLUMN_NAME_MACHINE = 'entrance';
    const NEW_COLUMN_NAME = 'Entrance';
    /**
     * Add a new column to the users list from admin interface
     *
     * @param string[] $column_headers The column headers name
     * 
     * @return string[]
     */
    public function modifyNumColumn($column_headers)
    {
        $column_headers[self::NEW_COLUMN_NAME_MACHINE] = self::NEW_COLUMN_NAME;
        return $column_headers;
    }

    /**
     * Adds the content for the new column
     *
     * @return void
     */
    public function addNewColumnContent($value, $column_name, $user_id)
    {
        if (self::NEW_COLUMN_NAME_MACHINE == $column_name) {
            return get_user_meta($user_id, 'registering_domain', true);
        }

        return $value;
    }
}