<?php

namespace danilocgsilva;

use danilocgsilva\Recorder;
use danilocgsilva\TableModifier;

class RegisteredFromDomain
{
    /**
     * WordPress Hook
     */
    public function hookToUserRegister()
    {
        \add_action('user_register', array(new Recorder(), "record"), 10, 1);
    }

    /**
     * Modify users table to add new Column
     *
     * @return void
     */
    public function modifyUserRow($tableModifier)
    {
        \add_action(
            'manage_users_columns', 
            array($tableModifier, "modifyNumColumn")
        );
        
        \add_action(
            'manage_users_custom_column', 
            array($tableModifier, "addNewColumnContent"), 
            10,
            3
        );
    }

    /**
     * Bootstrap actions
     */
    public static function runner()
    {
        $exec = new self();
        $exec->hookToUserRegister();
        $exec->modifyUserRow(new TableModifier());
    }
}