<?php

/**
 * Plugin Name: Registered from Domain
 * Description: Saves in the usermeta table the domain from which user has been registered. Usefull when different WordPress sites shares same users base.
 * Licence: MIT
 * Author: Danilo Silva (danilocgsilva)
 * Version: 1.0
 */

require_once("vendor/autoload.php");
danilocgsilva\RegisteredFromDomain::runner();
